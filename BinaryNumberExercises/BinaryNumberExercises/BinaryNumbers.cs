﻿using System;
using System.Text;

namespace BinaryNumberExercises
{
  public static class BinaryNumbers
  {

    private static string ConstructBinary(string binary)
    {
      if (string.IsNullOrEmpty(binary))
      {
        return string.Empty;
      }

      var stringBuilder = new StringBuilder();
      var size = 4 - binary.Length;
      if (size <= 0 )
      {
        return binary;
      }

      for (var i = 0; i < size; i++)
      {
        stringBuilder.Append("0");
      }

      stringBuilder.Append(binary);
      return stringBuilder.ToString();
    }

    //perform successive division 2 and take the remainder
    //and then read the remainders from bottom to top to get the binary number
    public static string ConvertDecimalToBinary(int decimalNumber)
    {
      if (decimalNumber == 0)
      {
        return "0000";
      }

      var binaryStringBuilder = new StringBuilder();
      var reversedBinaryStringBuilder = new StringBuilder();

      while (decimalNumber !=0)
      {
        var remainder = decimalNumber % 2;
        decimalNumber /= 2;
        binaryStringBuilder.Append(remainder);
      }

      var size = binaryStringBuilder.Length;
      for (var i = size-1; i >= 0; i--)
      {
        reversedBinaryStringBuilder.Append(binaryStringBuilder[i]);
      }

      return ConstructBinary(reversedBinaryStringBuilder.ToString());
    }

    public static int ConvertBinaryToDecimal(string binaryNumber)
    {
      if (string.IsNullOrEmpty(binaryNumber))
      {
        return -1;
      }

      if (binaryNumber == "0000")
      {
        return 0;
      }

      var sum = 0;
      var size = binaryNumber.Length;

      try
      {
        for(var i = size - 1; i >= 0; i--)
        {
          var digit = int.Parse($"{binaryNumber[i]}");
          sum += digit * (int)Math.Pow(2, (size - 1) - i);
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }

      return sum;
    }
  }
}
