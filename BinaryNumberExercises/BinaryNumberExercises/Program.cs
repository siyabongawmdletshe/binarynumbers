﻿using System;

namespace BinaryNumberExercises
{
  public class Program
  {
    public static void Main(string[] args)
    {
      //var decimalNumber = 5;
      //Console.WriteLine($"The binary number for {decimalNumber} : {BinaryNumbers.ConvertDecimalToBinary(decimalNumber)} ");
      
     
      for (var i = 0; i <= 10; i++ ) 
      {
        var binaryNumber = BinaryNumbers.ConvertDecimalToBinary(i);
        Console.WriteLine($"The decimal number for {binaryNumber} : {BinaryNumbers.ConvertBinaryToDecimal(binaryNumber)} ");
      }

      
    }
  }
}
